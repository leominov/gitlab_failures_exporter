package main

import (
	"flag"
	"fmt"
	"net/http"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
)

var (
	Version = "0.0.0"

	listenAddress = flag.String("web.listen-address", ":9201", "Address to listen on for web interface and telemetry.")
	metricsPath   = flag.String("web.telemetry-path", "/metrics", "Path under which to expose metrics.")
	logLevel      = flag.String("log-level", "INFO", "Level of logging.")
	projectsRaw   = flag.String("projects", "", "Project IDs or names separated by comma.")
	printsVersion = flag.Bool("version", false, "Prints version and exit.")
)

func main() {
	flag.Parse()

	if *printsVersion {
		fmt.Println(Version)
		return
	}

	if level, err := logrus.ParseLevel(*logLevel); err == nil {
		logrus.SetLevel(level)
	}

	projects, err := parseProjects(*projectsRaw)
	if err != nil {
		logrus.Fatal(err)
	}

	config, err := LoadConfigFromEnv()
	if err != nil {
		logrus.Fatal(err)
	}

	logrus.Infof("Staging gitlab_failures_exporter %s...", Version)
	logrus.Debugf("Configuration: %s", config.String())

	logrus.Infof("Listening on address: %s", *listenAddress)
	http.Handle(*metricsPath, promhttp.Handler())

	exporter, err := NewExporter(config, projects)
	if err != nil {
		logrus.Fatal(err)
	}

	err = prometheus.Register(exporter)
	if err != nil {
		logrus.WithError(err).Fatal("Error registering an collector")
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
             <head><title>GitLab Failures Exporter</title></head>
             <body>
             <h1>GitLab Failures Exporter</h1>
             <p><a href='` + *metricsPath + `'>Metrics</a></p>
             </body>
             </html>`))
	})
	if err := http.ListenAndServe(*listenAddress, nil); err != nil {
		logrus.WithError(err).Fatal("Error starting HTTP server")
	}
}

func parseProjects(raw string) ([]string, error) {
	var result []string
	ids := strings.Split(raw, ",")
	for _, id := range ids {
		result = append(result, strings.TrimSpace(id))
	}
	return result, nil
}
