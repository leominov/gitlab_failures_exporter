package main

import (
	"fmt"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

const namespace = "gitlab_failures_exporter"

type Exporter struct {
	config          *Config
	gitlabCli       *gitlab.Client
	listJobsOptions *gitlab.ListJobsOptions
	projects        []string
	lastFailure     map[string]time.Time
	lastScrapeError prometheus.Gauge
	failuresCounter *prometheus.CounterVec
}

func NewExporter(c *Config, projects []string) (*Exporter, error) {
	e := &Exporter{
		config:      c,
		projects:    projects,
		lastFailure: make(map[string]time.Time),
		lastScrapeError: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      "last_scrape_error",
			Help:      "The last scrape error status.",
		}),
		failuresCounter: prometheus.NewCounterVec(prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "failures_counter",
			Help:      "Jobs failures counter",
		}, []string{"project", "job"}),
	}
	e.listJobsOptions = &gitlab.ListJobsOptions{
		Scope: []gitlab.BuildStateValue{
			gitlab.Failed,
		},
	}
	err := e.setupGitLabClient()
	return e, err
}

func (e *Exporter) setupGitLabClient() error {
	var (
		cli *gitlab.Client
		err error
	)
	if len(e.config.GitlabToken) > 0 {
		cli, err = gitlab.NewClient(e.config.GitlabToken, gitlab.WithBaseURL(e.config.GitlabEndpoint))
	} else {
		cli, err = gitlab.NewBasicAuthClient(e.config.GitlabUsername, e.config.GitlabPassword, gitlab.WithBaseURL(e.config.GitlabEndpoint))
	}
	e.gitlabCli = cli
	return err
}

func (e *Exporter) Describe(ch chan<- *prometheus.Desc) {
	ch <- e.lastScrapeError.Desc()
	e.failuresCounter.Describe(ch)
}

func (e *Exporter) collectProject(project string) error {
	lastFailure, ok := e.lastFailure[project]
	if !ok {
		e.lastFailure[project] = time.Now()
		return nil
	}
	jobs, _, err := e.gitlabCli.Jobs.ListProjectJobs(project, e.listJobsOptions)
	if err != nil {
		return err
	}
	if len(jobs) == 0 {
		return nil
	}
	for i := len(jobs) - 1; i >= 0; i-- {
		job := jobs[i]
		if job.FinishedAt == nil || job.AllowFailure || job.FinishedAt.Before(lastFailure) {
			continue
		}
		e.lastFailure[project] = *job.FinishedAt
		e.failuresCounter.
			WithLabelValues(project, fmt.Sprintf("%s/%s", job.Stage, job.Name)).Inc()
	}
	return nil
}

func (e *Exporter) collect() error {
	for _, project := range e.projects {
		err := e.collectProject(project)
		if err != nil {
			return err
		}
	}
	return nil
}

func (e *Exporter) Collect(ch chan<- prometheus.Metric) {
	e.lastScrapeError.Set(0)

	err := e.collect()
	if err != nil {
		e.lastScrapeError.Set(1)
		logrus.WithError(err).Error("Failed to scrape data")
	}

	e.failuresCounter.Collect(ch)
}
