module github.com/leominov/gitlab_failures_exporter

go 1.14

require (
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/prometheus/client_golang v1.6.0
	github.com/sirupsen/logrus v1.6.0
	github.com/xanzy/go-gitlab v0.31.0
)
